package it.unibo.oop.lab06.generics1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Graphs {
	
	//gets a path from source to target
	public static <N> List<N> getBfsPath (GraphImpl<N> graph, N source, N target){
		Map<N,N> parent = new HashMap<>();
		Map<N,Boolean>  visited = new HashMap<>();
		Graphs.bfs(graph, source, parent, visited);
		List<N> path =  new ArrayList<>();
		path.add(target);
		N node = target;
		while(parent.get(node)!=null) {
			node = parent.get(node);
			path.add( 0,node);
		}
		return path;
	}

	//initializes a graph for a bfs
	private static <N> void initializeGraph(GraphImpl<N> graph, N source, Map<N,N> parent, Map<N,Boolean> visited, Queue<N> queue) {
		for(N node: graph.nodeSet()) {
			parent.put(node,null);
			visited.put(node,false);	
		}
		queue.add(source);
	}
	
	//executes a bfs
	public static <N> void bfs(GraphImpl<N> graph, N source, Map<N,N> parent, Map<N,Boolean> visited) {
		Queue<N> queue = new LinkedList<>();
		Graphs.initializeGraph(graph, source, parent, visited, queue);
		N node;
		while(!queue.isEmpty()) {
			node = queue.poll();
			for(N actual:graph.linkedNodes(node)) {
				if(visited.get(actual) == false) {
					parent.put(actual,node);
					queue.add(actual);
				}
			}
			visited.put(node, true);
		}
	}
	
}
