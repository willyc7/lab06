package it.unibo.oop.lab06.generics1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N> implements Graph<N> {

	private final Map<N,Set<N>> graph;
	
	public GraphImpl() {
		this.graph = new HashMap<N,Set<N>>();
	}
	
	@Override
	public void addNode(N node) {
		if(!this.graph.containsKey(node)) {
			this.graph.put(node, new HashSet<>());
		}
	}

	@Override
	public void addEdge(N source, N target) {
		//no additional controls because the add method controls if there is an equal element in the set
		this.graph.get(source).add(target);
	}

	@Override
	public Set<N> nodeSet() {
		return new HashSet<>(this.graph.keySet());
	}

	@Override
	public Set<N> linkedNodes(N node) {
		return new HashSet<>(this.graph.get(node));
	}

	@Override
	public List<N> getPath(N source, N target) {
		return Graphs.getBfsPath(this, source, target);
	}

}
