package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    	long time = System.nanoTime();
		List<Integer> list = new ArrayList<>();
		for (int i = 100; i < 200; i++) {
			list.add(0,i);
		}
		System.out.println("Tempo impiegato per array list " + (System.nanoTime() - time) + " ns");
		List<Integer> linked = new LinkedList<>(list);
		System.out.println(linked);
		//inverting
		int index = linked.size() - 1;
		int old = linked.get(index);
		linked.set(index, linked.get(0));
		linked.set(0, old);
		for(int number:linked) {
			System.out.print(number+" ");
		}
		System.out.println();
		//linked performance
		time = System.nanoTime();
		for (int i = 200; i < 300; i++) {
			linked.add(0,i);
		}
		System.out.println("Tempo impiegato per la linked list " + (System.nanoTime() - time) + " ns");
		
		for(int number:linked) {
			System.out.print(number+" ");
		}
		
		//reading performance
		time = System.nanoTime();
		for (int i = 0; i < 1000; i++) {
			linked.get(linked.size()/2);
		}
		System.out.println("Tempo impiegato per lettura nell'array list " + (System.nanoTime() - time) + " ns");
		
		time = System.nanoTime();
		for (int i = 0; i < 1000; i++) {
			linked.get(linked.size()/2);
		}
		System.out.println("Tempo impiegato per lettura nella linked list " + (System.nanoTime() - time) + " ns");
		
		Map<String,Long> world = new HashMap<>();
		world.put("Asia", 4298723000L);
		world.put("Europe", 742452000L);
		world.put("Africa", 1110635000L);
		world.put("Antartica", 0L);
		world.put("Americas", 972005000L);
		world.put("Oceania", 38304000L);
		long sum = 0L;
		for(long number:world.values()) {
			sum = sum + number;
		}
		System.out.println("La popolazione mondiale è "+sum);
    }
}
