package it.unibo.oop.lab.exception1;

/**
 * Represents an exception occurring when a robot tries to perform
 * an operation without enough battery
 * 
 */
public class NotEnoughBatteryException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final double batteryRequired;
	
	public NotEnoughBatteryException(final double batteryRequired) {
		super();
		this.batteryRequired = batteryRequired;
	}
	
	@Override
	public String toString() {
		return "Operation not possible.\n Battery required: " + this.batteryRequired + " .";
	}
}
