package it.unibo.oop.lab.exception2.exceptions;

public class TransactionOverQuotaException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TransactionOverQuotaException() {
		super();
	}
	
	public String toString() {
		return "*ATM transactions limit reached*";
	}

}
