package it.unibo.oop.lab.exception2.exceptions;

public class WrongAccountHolderException extends IllegalArgumentException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public WrongAccountHolderException() {
		super();
	}
	
	public String toString() {
		return "*Wrong user ID*";
	}

}
