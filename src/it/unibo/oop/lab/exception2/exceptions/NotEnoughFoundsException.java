package it.unibo.oop.lab.exception2.exceptions;

public class NotEnoughFoundsException extends IllegalStateException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotEnoughFoundsException() {
		super();
	}
	
	public String toString() {
		return "*Not enough founds to execute the operation*";
	}
}
