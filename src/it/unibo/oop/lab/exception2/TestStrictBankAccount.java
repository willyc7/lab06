package it.unibo.oop.lab.exception2;

import org.junit.Assert;
import org.junit.Test;

import it.unibo.oop.lab.exception2.exceptions.NotEnoughFoundsException;
import it.unibo.oop.lab.exception2.exceptions.TransactionOverQuotaException;
import it.unibo.oop.lab.exception2.exceptions.WrongAccountHolderException;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	AccountHolder ac1 = new AccountHolder( "Vez", "Gringo", 32);
    	StrictBankAccount s1 = new StrictBankAccount(32, 100, 3);
    	AccountHolder ac2 = new AccountHolder( "NONONO", "EH", 2);
    	StrictBankAccount s2 = new StrictBankAccount(2, 500, 2);
    	try {
    		s1.depositFromATM(3, 100);
    		Assert.fail();
    	} catch( WrongAccountHolderException e ) {
    		Assert.assertNotNull(e);
    	}
    	try {
    		s1.withdrawFromATM(32, 500);
    		Assert.fail();
    	} catch( NotEnoughFoundsException e ) {
    		Assert.assertNotNull(e);
    	}
    	s1.withdrawFromATM(32, 50);
    	s1.withdrawFromATM(32, 10);
    	s1.withdraw(32, 20);
    	try {
    		s1.depositFromATM( 32, 200);
    	} catch( TransactionOverQuotaException e ) {
    		Assert.fail();
    	}
    	try {
    		s1.depositFromATM( 32, 200);
    		Assert.fail();
    	} catch( TransactionOverQuotaException e ) {
    		Assert.assertNotNull(e);
    	}
    }
}
